package ftn.OWP.dao;

import java.util.List;


import ftn.OWP.model.Knjiga;

public interface KnjigaDAO {

	Knjiga findOne(String isbn);

	List<Knjiga> findAll();
	
	int save(Knjiga knjiga);
	
	List<Knjiga> find(String isbn, String naziv, Integer minCena, Integer maxCena, Integer ocena, 
			String autor, String jezik, String sort, String redosled);
	
	public int update(Knjiga knjiga);
}
