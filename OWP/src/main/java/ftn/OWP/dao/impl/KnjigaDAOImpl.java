package ftn.OWP.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ftn.OWP.dao.KnjigaDAO;
import ftn.OWP.model.Knjiga;
import ftn.OWP.model.Zanr;

@Repository
public class KnjigaDAOImpl implements KnjigaDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class KnjigaRowCallBackHandler implements RowCallbackHandler {

		private Map<String, Knjiga> knjige = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			String isbn = resultSet.getString(index++);
			String naziv = resultSet.getString(index++);
			String izdavackaKuca = resultSet.getString(index++);
			String autor = resultSet.getString(index++);
			String godinaIzdavanja = resultSet.getString(index++);
			String opis = resultSet.getString(index++);
			Integer cena = resultSet.getInt(index++);
			Integer brojStranica = resultSet.getInt(index++);
			String tipPoveza = resultSet.getString(index++);
			String pismo = resultSet.getString(index++);
			String jezik = resultSet.getString(index++);
			Integer ocena = resultSet.getInt(index++);
			Integer brojPrimeraka = resultSet.getInt(index++);
			
			Knjiga knjiga = knjige.get(isbn);
			if (knjiga == null) {
				knjiga = new Knjiga(isbn, naziv, izdavackaKuca, autor, godinaIzdavanja, opis, cena, brojStranica, tipPoveza, pismo, jezik, ocena, brojPrimeraka);
				knjige.put(knjiga.getIsbn(), knjiga); 
			}
		}

		public List<Knjiga> getKnjige() {
			return new ArrayList<>(knjige.values());
		}

	}
	
	private class KnjigaZanrRowCallBackHandler implements RowCallbackHandler {

		private Map<String, Knjiga> knjige = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			String isbn = resultSet.getString(index++);
			String naziv = resultSet.getString(index++);
			String izdavackaKuca = resultSet.getString(index++);
			String autor = resultSet.getString(index++);
			String godinaIzdavanja = resultSet.getString(index++);
			String opis = resultSet.getString(index++);
			Integer cena = resultSet.getInt(index++);
			Integer brojStranica = resultSet.getInt(index++);
			String tipPoveza = resultSet.getString(index++);
			String pismo = resultSet.getString(index++);
			String jezik = resultSet.getString(index++);
			Integer ocena = resultSet.getInt(index++);
			Integer brojPrimeraka = resultSet.getInt(index++);
			
			Knjiga knjiga = knjige.get(isbn);
			if (knjiga == null) {
				knjiga = new Knjiga(isbn, naziv, izdavackaKuca, autor, godinaIzdavanja, opis, cena, brojStranica, tipPoveza, pismo, jezik, ocena, brojPrimeraka);
				knjige.put(knjiga.getIsbn(), knjiga); 
			}
			
			
			Integer zanrId=resultSet.getInt(index++);
			String zanrIme=resultSet.getString(index++);

			if (zanrIme != null) {
				String zanrOpis=resultSet.getString(index++);
				Zanr zanr=new Zanr(zanrId,zanrIme, zanrOpis);
				knjiga.getZanrovi().add(zanr);
			}
		}

		public List<Knjiga> getKnjige() {
			return new ArrayList<>(knjige.values());
		}

	}
	
	@Override
	public Knjiga findOne(String isbn) {
		String sql = 
				"SELECT k.isbn, k.naziv, k.izdavackaKuca, k.autor, k.godinaIzdavanja, "
				+ "k.opis, k.cena, k.brojStranica, k.tipPoveza, k.pismo, k.jezik, k.ocena, k.brojPrimeraka, z.id, z.ime, z.opis "
				+ "FROM knjiga k "
				+ "LEFT JOIN knjiga_zanr kz ON kz.knjigaIsbn = k.isbn "
				+ "LEFT JOIN zanr z ON kz.zanrId = z.id " 
				+ "WHERE k.isbn = ? " 
				+ "ORDER BY k.isbn";

		KnjigaZanrRowCallBackHandler rowCallbackHandler = new KnjigaZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, isbn);

		return rowCallbackHandler.getKnjige().get(0);
	}
	
	@Override
	public List<Knjiga> findAll() {
		String sql = 
				"SELECT k.isbn, k.naziv, k.izdavackaKuca, k.autor, k.godinaIzdavanja, "
				+ "k.opis, k.cena, k.brojStranica, k.tipPoveza, k.pismo, k.jezik, k.ocena "
				+ "FROM knjiga k "
				+ "ORDER BY k.isbn"; 

		KnjigaRowCallBackHandler rowCallbackHandler = new KnjigaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		List<Knjiga> rez = rowCallbackHandler.getKnjige();
		
		return rez;
	}
	
	@Transactional
	@Override
	public int save(Knjiga knjiga) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO knjiga (isbn, naziv, izdavackaKuca, autor, godinaIzdavanja, opis," 
						+ "cena, brojStranica, tipPoveza, pismo, jezik, ocena) "
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql);
				int index = 1;
				preparedStatement.setString(index++, knjiga.getIsbn());
				preparedStatement.setString(index++, knjiga.getNaziv());
				preparedStatement.setString(index++, knjiga.getIzdavackaKuca());
				preparedStatement.setString(index++, knjiga.getAutor());
				preparedStatement.setString(index++, knjiga.getGodinaIzdavanja());
				preparedStatement.setString(index++, knjiga.getOpis());
				preparedStatement.setInt(index++, knjiga.getCena());
				preparedStatement.setInt(index++, knjiga.getBrojStranica());
				preparedStatement.setString(index++, knjiga.getTipPoveza());
				preparedStatement.setString(index++, knjiga.getPismo());
				preparedStatement.setString(index++, knjiga.getJezik());
				preparedStatement.setInt(index++, knjiga.getOcena());

				return preparedStatement;
			}

		};
		
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator) == 1;
		if (uspeh) {
			String sqlForZanr="INSERT INTO knjiga_zanr(zanrId, knjigaIsbn) values(?,?)";
			for(Zanr zanrOne : knjiga.getZanrovi()) {
				uspeh=uspeh&&jdbcTemplate.update(sqlForZanr, zanrOne.getId(), knjiga.getIsbn())==1;
			}
		}
		
		
		return uspeh?1:0;
	}
	
	@Override
	public List<Knjiga> find(String isbn, String naziv, Integer minCena, Integer maxCena, Integer ocena, 
			String autor, String jezik, String sort, String redosled) {
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT k.isbn, k.naziv, k.cena, k.ocena, k.autor, k.jezik FROM knjiga k "; 
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		String orderBySql = "";
		
		boolean imaArgumenata = false;
		
		if(isbn!=null && !isbn.isEmpty()) {
			isbn = "%" + isbn + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.isbn LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(isbn);
		}
		
		if(naziv!=null && !naziv.isEmpty()) {
			naziv = "%" + naziv + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.naziv LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(naziv);
		}

		if(minCena!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.cena >= ?");
			imaArgumenata = true;
			listaArgumenata.add(minCena);
		}
		
		if(maxCena!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.cena <= ?");
			imaArgumenata = true;
			listaArgumenata.add(maxCena);
		}
		if(ocena!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.ocena = ?");
			imaArgumenata = true;
			listaArgumenata.add(ocena);
		}
		if(autor!=null && !autor.isEmpty()) {
			autor = "%" + autor + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.autor LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(autor);
		}
		if(jezik!=null && !jezik.isEmpty()) {
			jezik = "%" + jezik + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.jezik LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(jezik);
		}
		
		if(isbn==null || isbn.isEmpty()) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.brojPrimeraka > 0");
			imaArgumenata = true;
		}

		if (sort != null && !sort.isEmpty()) {
			orderBySql = " ORDER BY ";
			if (sort.equals("naziv")) {
				orderBySql += "k.naziv";
			} else if (sort.equals("cena")) {
				orderBySql += "k.cena";
			} else if (sort.equals("ocena")) {
				orderBySql += "k.ocena";
			} else if (sort.equals("autor")) {
				orderBySql += "k.autor";
			} else if (sort.equals("jezik")) {
				orderBySql += "k.jezik";
			}
			if (redosled != null && !redosled.isEmpty()) {
				if (redosled.equals("rastuce")) {
					orderBySql += " asc";
				} else {
					orderBySql += " desc";
				}
			}
		} 
		
		if(imaArgumenata)
			sql=sql + whereSql.toString() + orderBySql; //+" ORDER BY k.isbn";
		else
			sql=sql + " ORDER BY k.isbn";
		
		@SuppressWarnings("deprecation")
		List<Knjiga> knjige = jdbcTemplate.query(sql, listaArgumenata.toArray(), new KnjigaRowMapper());
		return knjige;
	}
	
	private class KnjigaRowMapper implements RowMapper<Knjiga> {

		@Override
		public Knjiga mapRow(ResultSet resultSet, int rowNum) throws SQLException {
			int index = 1;
			String isbn = resultSet.getString(index++);
			String naziv = resultSet.getString(index++);
			Integer cena = resultSet.getInt(index++);
			Integer ocena = resultSet.getInt(index++);
			String autor = resultSet.getString(index++);
			String jezik = resultSet.getString(index++);

			Knjiga knjiga = new Knjiga(isbn, naziv, cena, ocena, autor, jezik);
			return knjiga;
		}

	}
	
	@Transactional
	@Override
	public int update(Knjiga knjiga) {
		
		String sqlDeleteZanrovi = "DELETE FROM knjiga_zanr WHERE knjigaIsbn = ?";
		jdbcTemplate.update(sqlDeleteZanrovi, knjiga.getIsbn());

		boolean uspeh = true;
		if (knjiga.getZanrovi() != null && !knjiga.getZanrovi().isEmpty()) {
			String insertZanrovi="INSERT INTO knjiga_zanr (zanrId, knjigaIsbn) values(?,?)";
			for(Zanr itZanr:knjiga.getZanrovi()) {
				uspeh = uspeh && jdbcTemplate.update(insertZanrovi, itZanr.getId(), knjiga.getIsbn())==1;
			}
		}
		
		String sql = "UPDATE knjiga SET naziv = ?, izdavackaKuca = ?, autor = ?, "
				+ "godinaIzdavanja = ?, opis = ?, cena = ?, brojStranica = ?, "
				+ "tipPoveza = ?, pismo = ?, jezik = ?, ocena = ?, brojPrimeraka = ? WHERE isbn = ?";	
		uspeh = uspeh &&  jdbcTemplate.update(sql, knjiga.getNaziv(), knjiga.getIzdavackaKuca(), knjiga.getAutor(),
											knjiga.getGodinaIzdavanja(), knjiga.getOpis(), knjiga.getCena(),
											knjiga.getBrojStranica(), knjiga.getTipPoveza(), knjiga.getPismo(),
											knjiga.getJezik(), knjiga.getOcena(), knjiga.getBrojPrimeraka(), knjiga.getIsbn()) == 1;

		return uspeh?1:0;
	}
	
}
