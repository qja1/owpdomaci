package ftn.OWP.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import ftn.OWP.dao.KorisnikDAO;
import ftn.OWP.model.Korisnik;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class KorisnikRowMapper implements RowMapper<Korisnik> {

		@Override
		public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			String korisnickoIme = rs.getString(index++);
			String lozinka = rs.getString(index++);
			String email = rs.getString(index++);
			String ime = rs.getString(index++);
			String prezime = rs.getString(index++);
			Date datumRodjenja = rs.getDate(index++);
			String adresa = rs.getString(index++);
			String brojTelefona = rs.getString(index++);
			Date datumVremeRegistracije = rs.getDate(index++);
			Boolean administrator = rs.getBoolean(index++);

			Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa,
					brojTelefona, datumVremeRegistracije, administrator);
			return korisnik;
		}

	}

	@Override
	public Korisnik findOne(String korisnickoIme) {
		try {
			String sql = "SELECT korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumVremeRegistracije, administrator FROM korisnik WHERE korisnickoIme = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		try {
			String sql = "SELECT korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumVremeRegistracije, administrator FROM korisnik WHERE korisnickoIme = ? AND lozinka = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme, lozinka);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}

	@Override
	public List<Korisnik> findAll() {
		String sql = "SELECT korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumVremeRegistracije, administrator FROM korisnik";
		return jdbcTemplate.query(sql, new KorisnikRowMapper());
	}

	@Override
	public void save(Korisnik korisnik) {
		String sql = "INSERT INTO korisnik (korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, datumVremeRegistracije, administrator) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		jdbcTemplate.update(sql, korisnik.getKorisnickoIme(), korisnik.getLozinka(), korisnik.getEmail(),
				korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(),
				korisnik.getBrojTelefona(), korisnik.getDatumVremeRegistracije(), korisnik.isAdministrator());
	}

	@Override
	public void update(Korisnik korisnik) {
		if (korisnik.getLozinka() == null) {
			String sql = "UPDATE korisnik SET email = ?, ime = ?, prezime = ?, datumRodjenja = ?, adresa = ?, brojTelefona = ?, administrator = ? WHERE korisnickoIme = ?";
			jdbcTemplate.update(sql, korisnik.getEmail(), korisnik.getIme(), korisnik.getPrezime(),
					korisnik.getDatumRodjenja(), korisnik.getAdresa(), korisnik.getBrojTelefona(),
					korisnik.isAdministrator(), korisnik.getKorisnickoIme());
		} else {
			String sql = "UPDATE korisnici SET lozinka = ?, email = ?, ime = ?, prezime = ?, datumRodjenja = ?, adresa = ?, brojTelefona = ?, administrator = ? WHERE korisnickoIme = ?";
			jdbcTemplate.update(sql, korisnik.getLozinka(), korisnik.getEmail(), korisnik.getIme(),
					korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(),
					korisnik.getBrojTelefona(), korisnik.isAdministrator(), korisnik.getKorisnickoIme());
		}
	}

	@Override
	public void delete(String korisnickoIme) {
		String sql = "DELETE FROM korisnik WHERE korisnickoIme = ?";
		jdbcTemplate.update(sql, korisnickoIme);
	}

}
