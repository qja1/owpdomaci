package ftn.OWP.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import ftn.OWP.dao.ZanrDAO;
import ftn.OWP.model.Zanr;

@Repository
public class ZanrDAOImpl implements ZanrDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class ZanrRowMapper implements RowMapper<Zanr> {

		@Override
		public Zanr mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			int id = rs.getInt(index++);
			String ime = rs.getString(index++);
			String opis = rs.getString(index++);
			
			Zanr zanr = new Zanr(id, ime, opis);
			return zanr;
		}

	}
	
	@Override
	public List<Zanr> findAll() {
		String sql = "SELECT id, ime, opis FROM zanr";
		return jdbcTemplate.query(sql, new ZanrRowMapper());
	}
	
	@Override
	public Zanr findOne(Integer id) {
		String sql = "SELECT id, ime, opis FROM zanr WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new ZanrRowMapper(), id);
	}
	
}
