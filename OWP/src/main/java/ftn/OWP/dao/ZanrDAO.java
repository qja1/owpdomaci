package ftn.OWP.dao;

import java.util.List;

import ftn.OWP.model.Zanr;

public interface ZanrDAO {

	public List<Zanr> findAll();
	
	public Zanr findOne(Integer id);
	
}
