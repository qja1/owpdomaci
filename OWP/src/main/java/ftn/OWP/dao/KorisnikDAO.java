package ftn.OWP.dao;

import java.util.List;

import ftn.OWP.model.Korisnik;

public interface KorisnikDAO {
	
	public Korisnik findOne(String korisnickoIme);

	public Korisnik findOne(String korisnickoIme, String lozinka);

	public List<Korisnik> findAll();

	public void save(Korisnik korisnik);

	public void update(Korisnik korisnik);

	public void delete(String korisnickoIme);

}
