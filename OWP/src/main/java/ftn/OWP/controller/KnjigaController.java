package ftn.OWP.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import ftn.OWP.model.Knjiga;
import ftn.OWP.model.Korisnik;
import ftn.OWP.model.Zanr;
import ftn.OWP.service.KnjigaService;
import ftn.OWP.service.ZanrService;


@Controller
@RequestMapping(value="/Knjiga")
public class KnjigaController implements ServletContextAware {
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;

	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private ZanrService zanrService;
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping
	public ModelAndView findAll(
			@RequestParam(required=false) String isbn, 
			@RequestParam(required=false) String naziv, 
			@RequestParam(required=false) Integer minCena, 
			@RequestParam(required=false) Integer maxCena,
			@RequestParam(required=false) Integer ocena,
			@RequestParam(required=false) String autor,
			@RequestParam(required=false) String jezik, 
			@RequestParam(required=false) String sort,
			@RequestParam(required=false) String redosled,
			HttpSession session)  throws IOException {
		
		List<Knjiga> knjige = knjigaService.find(isbn, naziv, minCena, maxCena, ocena, autor, jezik, sort, redosled);

		ModelAndView rezultat = new ModelAndView("knjige");
		rezultat.addObject("knjige", knjige);

		return rezultat;
	}
	
	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam String isbn, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Knjiga knjiga = knjigaService.findOne(isbn);
		List<Zanr> zanrovi=zanrService.findAll();
		
		ModelAndView rezultat = new ModelAndView("knjiga");
		rezultat.addObject("knjiga", knjiga);
		rezultat.addObject("zanrovi",zanrovi);

		return rezultat;
	}
	
	@GetMapping(value="/Create")
	public ModelAndView create(HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjiga");
			return null;
		}
		
		List<Zanr> zanrovi = zanrService.findAll();

		ModelAndView rezultat = new ModelAndView("dodavanjeKnjige");
		rezultat.addObject("zanrovi", zanrovi);
		
		return rezultat;
	}

	@PostMapping(value="/Create")
	public void create(@RequestParam String isbn, @RequestParam String naziv, @RequestParam String izdavackaKuca, 
			@RequestParam String autor, @RequestParam String godinaIzdavanja, @RequestParam String opis,
			@RequestParam Integer cena, @RequestParam Integer brojStranica, @RequestParam String tipPoveza,
			@RequestParam String pismo, @RequestParam String jezik, @RequestParam Integer ocena,
			@RequestParam(name="zanrId", required=false) Integer[] zanroviId,
			HttpSession session, HttpServletResponse response) throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjiga");
			return;
		}

		Knjiga knjiga = new Knjiga(isbn, naziv, izdavackaKuca, autor, godinaIzdavanja, opis,
				cena, brojStranica, tipPoveza, pismo, jezik, ocena);
		knjiga.setZanrovi(zanrService.findById(zanroviId));
		knjigaService.save(knjiga);
		
		
		response.sendRedirect(baseURL + "Knjiga");
	}
	
	@PostMapping(value="/Edit")
	public void edit(@RequestParam String isbn, @RequestParam String naziv, @RequestParam String izdavackaKuca, 
			@RequestParam String autor, @RequestParam String godinaIzdavanja, @RequestParam String opis,
			@RequestParam Integer cena, @RequestParam Integer brojStranica, @RequestParam String tipPoveza,
			@RequestParam String pismo, @RequestParam String jezik, @RequestParam Integer ocena, @RequestParam(name="zanrId", required=false) Integer[] zanroviId,
			HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisnikController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.isAdministrator()) {
			response.sendRedirect(baseURL + "Knjiga");
			return;
		}

		Knjiga knjiga = knjigaService.findOne(isbn);
		if (knjiga == null) {
			response.sendRedirect(baseURL + "Knjiga");
			return;
		}	
		if (naziv == null || naziv.equals("") || 
			izdavackaKuca == null || izdavackaKuca.equals("") ||
			autor == null || autor.equals("") || 
			godinaIzdavanja == null || godinaIzdavanja.equals("") || 
			opis == null || opis.equals("") || 
			cena == null || cena.equals("") || 
			brojStranica == null || 
			tipPoveza == null || tipPoveza.equals("") || 
			pismo == null || pismo.equals("") || 
			jezik == null || jezik.equals("") || 
			ocena == null) {
			response.sendRedirect(baseURL + "Knjiga/Details?isbn=" + isbn);
			return;
		}

		knjiga.setNaziv(naziv);
		knjiga.setIzdavackaKuca(izdavackaKuca);
		knjiga.setAutor(autor);
		knjiga.setGodinaIzdavanja(godinaIzdavanja);
		knjiga.setOpis(opis);
		knjiga.setCena(cena);
		knjiga.setBrojStranica(brojStranica);
		knjiga.setTipPoveza(tipPoveza);
		knjiga.setPismo(pismo);
		knjiga.setJezik(jezik);
		knjiga.setOcena(ocena);
		knjiga.setZanrovi(zanrService.findById(zanroviId));
		
		knjigaService.update(knjiga);

		response.sendRedirect(baseURL + "Knjiga");
	}
	
	@GetMapping(value="/Poruci")
	public ModelAndView poruci(@RequestParam String isbn, @RequestParam Integer brojPorucenihPrimeraka,
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		if (brojPorucenihPrimeraka == null) {
			response.sendRedirect(baseURL + "Knjiga/Details?isbn=" + isbn);
			return null;
		}
		Knjiga knjiga = knjigaService.findOne(isbn);
		List<Zanr> zanrovi=zanrService.findAll();
		
		knjigaService.poruci(knjiga, brojPorucenihPrimeraka);
		
		ModelAndView rezultat = new ModelAndView("knjiga");
		rezultat.addObject("knjiga", knjiga);
		rezultat.addObject("zanrovi",zanrovi);
		rezultat.addObject("poruka", "Uspesno ste porucili kolicinu od " + brojPorucenihPrimeraka + " primeraka knjige " + knjiga.getNaziv() + "!");

		return rezultat;
	}
	
}
