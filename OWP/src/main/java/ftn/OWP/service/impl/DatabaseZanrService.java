package ftn.OWP.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftn.OWP.dao.ZanrDAO;
import ftn.OWP.model.Zanr;
import ftn.OWP.service.ZanrService;

@Service
public class DatabaseZanrService implements ZanrService {

	@Autowired
	private ZanrDAO zanrDAO;
	
	@Override
	public List<Zanr> findAll() {
		return zanrDAO.findAll();
	}
	
	@Override
	public List<Zanr> findById(Integer[] zanroviId) {
		List<Zanr> rezultat = new ArrayList<>();
		if (zanroviId != null) {
			for (Integer id: zanroviId) {
				Zanr zanr = zanrDAO.findOne(id);
				rezultat.add(zanr);
			}
		}

		return rezultat;
	}
	
}
