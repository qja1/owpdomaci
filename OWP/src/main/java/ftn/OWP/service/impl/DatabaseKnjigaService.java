package ftn.OWP.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ftn.OWP.dao.KnjigaDAO;
import ftn.OWP.model.Knjiga;
import ftn.OWP.service.KnjigaService;


@Service
public class DatabaseKnjigaService implements KnjigaService {

	@Autowired
	private KnjigaDAO knjigaDAO;
	
	@Override
	public Knjiga findOne(String isbn) {
		return knjigaDAO.findOne(isbn);
	}

	@Override
	public List<Knjiga> findAll() {
		return knjigaDAO.findAll();
	}
	
	@Transactional
	@Override
	public Knjiga save(Knjiga knjiga) {
		knjigaDAO.save(knjiga);
		return knjiga;
	}
	
	@Override
	public List<Knjiga> find(String isbn, String naziv, Integer minCena, Integer maxCena, Integer ocena, String autor,
			String jezik, String sort, String redosled) {
		return knjigaDAO.find(isbn, naziv, minCena, maxCena, ocena, autor, jezik, sort, redosled);
	}
	
	@Override
	public Knjiga update(Knjiga knjiga) {
		knjigaDAO.update(knjiga);
		return knjiga;
	}
	
	@Override
	public void poruci(Knjiga knjiga, int brojPorucenihPrimeraka) {
		knjiga.setBrojPrimeraka(knjiga.getBrojPrimeraka() + brojPorucenihPrimeraka);
		knjigaDAO.update(knjiga);
	}
	
}
