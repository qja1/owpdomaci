package ftn.OWP.service;

import java.util.List;

import ftn.OWP.model.Knjiga;

public interface KnjigaService {
	
	Knjiga findOne(String isbn);
	List<Knjiga> findAll();
	Knjiga save(Knjiga knjiga);
	Knjiga update(Knjiga knjiga);
	List<Knjiga> find(String isbn, String naziv, Integer minCena, Integer maxCena, 
			Integer ocena, String autor, String jezik, String sort, String redosled);
	void poruci(Knjiga knjiga, int brojPorucenihPrimeraka);
}
