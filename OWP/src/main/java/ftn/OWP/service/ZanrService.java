package ftn.OWP.service;

import java.util.List;

import ftn.OWP.model.Zanr;

public interface ZanrService {

	List<Zanr> findAll();
	List<Zanr> findById(Integer[] zanroviId);
}
