package ftn.OWP.model;

import java.util.Date;

public class Komentar {

	private int id;
	private String tekst;
	private int ocena;
	private Date datum;
	private Korisnik autorKomentara;
	private Status status;
	private Knjiga knjiga;
	
	public Komentar() {}
	
	public Komentar(int id, String tekst, int ocena, Date datum, Korisnik autorKomentara, Status status,
			Knjiga knjiga) {
		super();
		this.id = id;
		this.tekst = tekst;
		this.ocena = ocena;
		this.datum = datum;
		this.autorKomentara = autorKomentara;
		this.status = status;
		this.knjiga = knjiga;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public int getOcena() {
		return ocena;
	}

	public void setOcena(int ocena) {
		this.ocena = ocena;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public Korisnik getAutorKomentara() {
		return autorKomentara;
	}

	public void setAutorKomentara(Korisnik autorKomentara) {
		this.autorKomentara = autorKomentara;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}
	
	
	
	
}
