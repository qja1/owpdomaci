package ftn.OWP.model;

public class Status {

	private int id;
	private String vrstaStatusa;
	
	public Status() {}
	
	public Status(int id, String vrstaStatusa) {
		super();
		this.id = id;
		this.vrstaStatusa = vrstaStatusa;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVrstaStatusa() {
		return vrstaStatusa;
	}

	public void setVrstaStatusa(String vrstaStatusa) {
		this.vrstaStatusa = vrstaStatusa;
	}
	
	
	
	
}
