package ftn.OWP.model;

import java.util.Date;

public class Kupovina {

	private int id;
	private Date datum;
	private int ukupanBrojKnjiga;
	private Korisnik korisnikKupio;
	private String ukupnaCena;
	
	
	public Kupovina() {}
	
	public Kupovina(int id, Date datum, int ukupanBrojKnjiga, Korisnik korisnikKupio, String ukupnaCena) {
		super();
		this.id = id;
		this.datum = datum;
		this.ukupanBrojKnjiga = ukupanBrojKnjiga;
		this.korisnikKupio = korisnikKupio;
		this.ukupnaCena = ukupnaCena;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public int getUkupanBrojKnjiga() {
		return ukupanBrojKnjiga;
	}
	public void setUkupanBrojKnjiga(int ukupanBrojKnjiga) {
		this.ukupanBrojKnjiga = ukupanBrojKnjiga;
	}
	public Korisnik getKorisnikKupio() {
		return korisnikKupio;
	}
	public void setKorisnikKupio(Korisnik korisnikKupio) {
		this.korisnikKupio = korisnikKupio;
	}
	public String getUkupnaCena() {
		return ukupnaCena;
	}
	public void setUkupnaCena(String ukupnaCena) {
		this.ukupnaCena = ukupnaCena;
	}
	
	
	
}
