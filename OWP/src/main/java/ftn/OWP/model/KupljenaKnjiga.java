package ftn.OWP.model;

public class KupljenaKnjiga {
	
	private int id;
	private int brojPrimeraka;
	private String cena;
	private Kupovina kupovina;
	private Knjiga knjiga;
	
	public KupljenaKnjiga() {}
	
	public KupljenaKnjiga(int id, int brojPrimeraka, String cena, Kupovina kupovina, Knjiga knjiga) {
		super();
		this.id = id;
		this.brojPrimeraka = brojPrimeraka;
		this.cena = cena;
		this.kupovina = kupovina;
		this.knjiga = knjiga;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBrojPrimeraka() {
		return brojPrimeraka;
	}

	public void setBrojPrimeraka(int brojPrimeraka) {
		this.brojPrimeraka = brojPrimeraka;
	}

	public String getCena() {
		return cena;
	}

	public void setCena(String cena) {
		this.cena = cena;
	}

	public Kupovina getKupovina() {
		return kupovina;
	}

	public void setKupovina(Kupovina kupovina) {
		this.kupovina = kupovina;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}
	
	
	

}
