package ftn.OWP.model;

import java.util.ArrayList;
import java.util.List;

public class Knjiga {

	//prvi commit i pocetak rada :D
	
	private String isbn;
	private String naziv;
	private String izdavackaKuca;
	private String autor;
	private String godinaIzdavanja;
	private String opis;
	private Integer cena;
	private int brojStranica;
	private String tipPoveza;
	private String pismo;
	private String jezik;
	private int ocena;
	private int brojPrimeraka;
	
	private List<Zanr> zanrovi=new ArrayList<>();
	
	public Knjiga() {}
	
	public Knjiga(String isbn, String naziv, String izdavackaKuca, String autor, String godinaIzdavanja, String opis,
			Integer cena, int brojStranica, String tipPoveza, String pismo, String jezik, int ocena) {
		super();
		this.isbn = isbn;
		this.naziv = naziv;
		this.izdavackaKuca = izdavackaKuca;
		this.autor = autor;
		this.godinaIzdavanja = godinaIzdavanja;
		this.opis = opis;
		this.cena = cena;
		this.brojStranica = brojStranica;
		this.tipPoveza = tipPoveza;
		this.pismo = pismo;
		this.jezik = jezik;
		this.ocena = ocena;
	}
	
	public Knjiga(String isbn, String naziv, String izdavackaKuca, String autor, String godinaIzdavanja, String opis,
			Integer cena, int brojStranica, String tipPoveza, String pismo, String jezik, int ocena, int brojPrimeraka) {
		super();
		this.isbn = isbn;
		this.naziv = naziv;
		this.izdavackaKuca = izdavackaKuca;
		this.autor = autor;
		this.godinaIzdavanja = godinaIzdavanja;
		this.opis = opis;
		this.cena = cena;
		this.brojStranica = brojStranica;
		this.tipPoveza = tipPoveza;
		this.pismo = pismo;
		this.jezik = jezik;
		this.ocena = ocena;
		this.brojPrimeraka = brojPrimeraka;
	}
	
	public Knjiga(String isbn, String naziv, Integer cena,  int ocena, String autor,  String jezik) {
		super();
		this.isbn = isbn;
		this.naziv = naziv;
		this.autor = autor;
		this.cena = cena;
		this.jezik = jezik;
		this.ocena = ocena;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getIzdavackaKuca() {
		return izdavackaKuca;
	}

	public void setIzdavackaKuca(String izdavackaKuca) {
		this.izdavackaKuca = izdavackaKuca;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getGodinaIzdavanja() {
		return godinaIzdavanja;
	}

	public void setGodinaIzdavanja(String godinaIzdavanja) {
		this.godinaIzdavanja = godinaIzdavanja;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Integer getCena() {
		return cena;
	}

	public void setCena(Integer cena) {
		this.cena = cena;
	}

	public int getBrojStranica() {
		return brojStranica;
	}

	public void setBrojStranica(int brojStranica) {
		this.brojStranica = brojStranica;
	}

	public String getTipPoveza() {
		return tipPoveza;
	}

	public void setTipPoveza(String tipPoveza) {
		this.tipPoveza = tipPoveza;
	}

	public String getPismo() {
		return pismo;
	}

	public void setPismo(String pismo) {
		this.pismo = pismo;
	}

	public String getJezik() {
		return jezik;
	}

	public void setJezik(String jezik) {
		this.jezik = jezik;
	}

	public int getOcena() {
		return ocena;
	}

	public void setOcena(int ocena) {
		this.ocena = ocena;
	}

	public int getBrojPrimeraka() {
		return brojPrimeraka;
	}

	public void setBrojPrimeraka(int brojPrimeraka) {
		this.brojPrimeraka = brojPrimeraka;
	}

	public List<Zanr> getZanrovi() {
		return zanrovi;
	}

	public void setZanrovi(List<Zanr> zanrovi) {
		this.zanrovi = zanrovi;
	}
	
}
