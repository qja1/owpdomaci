package ftn.OWP.model;

public class LoyaltyKartica {

	private int id;
	private String popust;
	private int brojPojena;
	
	public LoyaltyKartica() {}
	
	public LoyaltyKartica(int id, String popust, int brojPojena) {
		super();
		this.id = id;
		this.popust = popust;
		this.brojPojena = brojPojena;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPopust() {
		return popust;
	}

	public void setPopust(String popust) {
		this.popust = popust;
	}

	public int getBrojPojena() {
		return brojPojena;
	}

	public void setBrojPojena(int brojPojena) {
		this.brojPojena = brojPojena;
	}
	
	
}
