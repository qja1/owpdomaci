CREATE SCHEMA `bibliotekaproba` ;
USE bibliotekaproba;

CREATE TABLE `knjiga` (
  `isbn` varchar(45) NOT NULL,
  `naziv` varchar(45) DEFAULT NULL,
  `izdavackaKuca` varchar(45) DEFAULT NULL,
  `autor` varchar(45) DEFAULT NULL,
  `godinaIzdavanja` varchar(45) DEFAULT NULL,
  `opis` varchar(45) DEFAULT NULL,
  `cena` int DEFAULT NULL,
  `brojStranica` int DEFAULT NULL,
  `tipPoveza` varchar(45) DEFAULT NULL,
  `pismo` varchar(45) DEFAULT NULL,
  `jezik` varchar(45) DEFAULT NULL,
  `ocena` int DEFAULT NULL,
  `brojPrimeraka` int DEFAULT NULL,
  PRIMARY KEY (`isbn`),
  UNIQUE KEY `isbn_UNIQUE` (`isbn`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `korisnik` (
  `korisnickoIme` varchar(45) NOT NULL,
  `lozinka` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `ime` varchar(45) DEFAULT NULL,
  `prezime` varchar(45) DEFAULT NULL,
  `datumRodjenja` date DEFAULT NULL,
  `adresa` varchar(45) DEFAULT NULL,
  `brojTelefona` varchar(45) DEFAULT NULL,
  `datumVremeRegistracije` datetime DEFAULT NULL,
  `administrator` tinyint DEFAULT '0',
  PRIMARY KEY (`korisnickoIme`),
  UNIQUE KEY `korisnickoIme_UNIQUE` (`korisnickoIme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `status` (
  `id` int NOT NULL,
  `vrstaStatusa` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `zanr` (
  `id` int NOT NULL,
  `ime` varchar(45) DEFAULT NULL,
  `opis` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='		';

CREATE TABLE `knjiga_zanr` (
  `zanrId` int NOT NULL,
  `knjigaIsbn` varchar(45) NOT NULL,
  PRIMARY KEY (`zanrId`,`knjigaIsbn`),
  KEY `fk_knjiga_zanr_knjiga_idx` (`knjigaIsbn`),
  CONSTRAINT `fk_knjiga_zanr_knjiga` FOREIGN KEY (`knjigaIsbn`) REFERENCES `knjiga` (`isbn`),
  CONSTRAINT `fk_knjiga_zanr_zanr` FOREIGN KEY (`zanrId`) REFERENCES `zanr` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `loyalty_kartica` (
  `id` int NOT NULL,
  `popust` varchar(45) DEFAULT NULL,
  `brojPoena` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `kupovina` (
  `id` int NOT NULL,
  `datum` date DEFAULT NULL,
  `ukupanBrojKnjiga` int DEFAULT NULL,
  `korisnikKupio` varchar(45) DEFAULT NULL,
  `ukupnaCena` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_korisnik_kupio_idx` (`korisnikKupio`),
  CONSTRAINT `fk_korisnik_kupio` FOREIGN KEY (`korisnikKupio`) REFERENCES `korisnik` (`korisnickoIme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `kupljena_knjiga` (
  `id` int NOT NULL,
  `brojPrimeraka` int DEFAULT NULL,
  `cena` varchar(45) DEFAULT NULL,
  `kupovina` int DEFAULT NULL,
  `knjiga` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_kupovina_idx` (`kupovina`),
  KEY `fk_kupljena_knjiga_knjiga_idx` (`knjiga`),
  CONSTRAINT `fk_kupljena_knjiga_knjiga` FOREIGN KEY (`knjiga`) REFERENCES `knjiga` (`isbn`),
  CONSTRAINT `fk_kupovina` FOREIGN KEY (`kupovina`) REFERENCES `kupovina` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `komentar` (
  `id` int NOT NULL,
  `tekst` varchar(45) DEFAULT NULL,
  `ocena` int DEFAULT NULL,
  `datum` date DEFAULT NULL,
  `autorKomentara` varchar(45) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `knjiga` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_komentar_status_idx` (`status`),
  KEY `fk_komentar_knjiga_idx` (`knjiga`),
  KEY `fk_komentar_autor_idx` (`autorKomentara`),
  CONSTRAINT `fk_komentar_autor` FOREIGN KEY (`autorKomentara`) REFERENCES `korisnik` (`korisnickoIme`),
  CONSTRAINT `fk_komentar_knjiga` FOREIGN KEY (`knjiga`) REFERENCES `knjiga` (`isbn`),
  CONSTRAINT `fk_komentar_status` FOREIGN KEY (`status`) REFERENCES `status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `bibliotekaproba`.`knjiga` 
(`isbn`, `naziv`, `izdavackaKuca`, `autor`, `godinaIzdavanja`, `opis`, `cena`, `brojStranica`, `tipPoveza`, `pismo`, `jezik`, `ocena`, `brojPrimeraka`) 
VALUES 
('1234567891234', 'Game of Thrones 1', 'Izdavacka kuca 1', 'George R. R. Martin', '2009', 'Opis got', '4000', '900', 'Tvrdi', 'Latinica', 'Engleski', '9', '40');

INSERT INTO `bibliotekaproba`.`knjiga` 
(`isbn`, `naziv`, `izdavackaKuca`, `autor`, `godinaIzdavanja`, `opis`, `cena`, `brojStranica`, `tipPoveza`, `pismo`, `jezik`, `ocena`, `brojPrimeraka`) 
VALUES 
('1231231231231', 'Zakon cutanja', 'Izdavacka kuca 2', 'Mario Puzo', '2006', 'Opis omerta', '2000', '200', 'Meki', 'Latinica', 'Srpski', '6', '30');

INSERT INTO `bibliotekaproba`.`knjiga` 
(`isbn`, `naziv`, `izdavackaKuca`, `autor`, `godinaIzdavanja`, `opis`, `cena`, `brojStranica`, `tipPoveza`, `pismo`, `jezik`, `ocena`, `brojPrimeraka`) 
VALUES 
('2342342342345', 'Idiot', 'Izdavacka kuca 3', 'Dostojevski', '1869', 'Opis idiota', '6000', '500', 'Tvrdi', 'Cilirica', 'Srpski', '5', '20');

INSERT INTO `bibliotekaproba`.`korisnik` (`korisnickoIme`, `lozinka`, `email`, `ime`, `prezime`, `datumRodjenja`, `adresa`, `brojTelefona`, `datumVremeRegistracije`, `administrator`) 
VALUES 
('igor', 'igor', 'igor@gmail.com', 'Igor', 'Kujacic', '2021-03-09', 'Adresa Igora', '065432345', '2021-03-12 22:44:34', '0');

INSERT INTO `bibliotekaproba`.`korisnik` (`korisnickoIme`, `lozinka`, `email`, `ime`, `prezime`, `datumRodjenja`, `adresa`, `brojTelefona`, `datumVremeRegistracije`, `administrator`) 
VALUES 
('admin', 'admin', 'admin@gmail.com', 'Admin', 'Administratovic', '2021-04-02', 'Adresa Admina', '065333345', '2021-01-12 23:44:34', '1');

INSERT INTO `bibliotekaproba`.`zanr` (`id`, `ime`, `opis`) 
VALUES 
('1', 'Drama', 'Opis drame');

INSERT INTO `bibliotekaproba`.`zanr` (`id`, `ime`, `opis`) 
VALUES 
('2', 'Biografija', 'Opis biografija');

INSERT INTO `bibliotekaproba`.`zanr` (`id`, `ime`, `opis`) 
VALUES 
('3', 'Roman', 'Opis romana');

INSERT INTO `bibliotekaproba`.`zanr` (`id`, `ime`, `opis`) 
VALUES 
('4', 'Fantastika', 'Opis fantastike');

INSERT INTO `bibliotekaproba`.`knjiga_zanr` (`zanrId`, `knjigaIsbn`) 
VALUES 
('1', '1234567891234');

INSERT INTO `bibliotekaproba`.`knjiga_zanr` (`zanrId`, `knjigaIsbn`) 
VALUES 
('2', '1234567891234');

INSERT INTO `bibliotekaproba`.`knjiga_zanr` (`zanrId`, `knjigaIsbn`) 
VALUES 
('3', '1231231231231');

INSERT INTO `bibliotekaproba`.`knjiga_zanr` (`zanrId`, `knjigaIsbn`) 
VALUES 
('1', '2342342342345');

INSERT INTO `bibliotekaproba`.`knjiga_zanr` (`zanrId`, `knjigaIsbn`) 
VALUES 
('2', '2342342342345');

INSERT INTO `bibliotekaproba`.`knjiga_zanr` (`zanrId`, `knjigaIsbn`) 
VALUES 
('4', '2342342342345');

